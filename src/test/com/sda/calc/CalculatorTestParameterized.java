package com.sda.calc;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(Parameterized.class)
public class CalculatorTestParameterized {

    private Calculator calculator;
    private Double valueA;
    private Double valueB;

    public CalculatorTestParameterized(Double valueA, Double valueB) {
        this.valueA = valueA;
        this.valueB = valueB;
    }

    @Before
    public void setup() {
        calculator = new Calculator();
    }

    @Parameterized.Parameters
    public static Collection getParameters() {
        return Arrays.asList(new Double[][]{{18.2, 94.1}, {123d, 3d}, {0.5, 9402.1}});
    }

    @Test
    public void add() {
        double result = calculator.add(valueA, valueB);
        assertThat(result).isEqualTo(valueA + valueB);
    }
}