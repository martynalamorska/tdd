package com.sda.calc;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.assertj.core.data.Offset;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class CalculatorTest {

    private static Calculator calculator;

    @BeforeClass
    public static void setup() {
        calculator = new Calculator();
    }

    @Test
    @Parameters({"18.2,94.1", "123,3", "0.5,9402.1"})
    public void add(double valueA, double valueB) {
        assertThat(calculator.add(valueA, valueB)).isEqualTo(valueA + valueB);
    }

    @Test
    @Parameters
    public void subtract(double valueA, double valueB) throws Exception {
        assertThat(calculator.subtract(valueA, valueB)).isNotNull().isEqualTo(valueA - valueB);
//        double subtract1 = calculator.subtract(34, 34);
//        double subtract2 = calculator.subtract(-123, -23.5);
//        double subtract3 = calculator.subtract(8, 97);
//        assertThat(subtract1).isEqualTo(0);
//        assertThat(subtract2).isNotEqualTo(0);
//        assertThat(subtract3).isEqualTo(-89);
    }

    private Object[] parametersForSubtract() {
        return new Object[]{
                new Object[]{34, 34},
                new Object[]{-123, 34},
                new Object[]{8, 97}
        };
    }

    @Test
    public void multiply() {

        double multiply1 = calculator.multiply(11.1, 11.1);
        double multiply2 = calculator.multiply(2, -0.1);
        double multiply3 = calculator.multiply(33, 10.01);
        assertThat(multiply1).isEqualTo(123.21);
        assertThat(multiply2).isEqualTo(-0.2);
        assertThat(multiply3).isNotEqualTo(0);
    }

    @Test
    public void divide() {

        double divide1 = calculator.divide(9, 3);
        double divide2 = calculator.divide(11, 0.2);
        double divide3 = calculator.divide(-0.33, 0.03);
        assertThat(divide1).isEqualTo(3);
        assertThat(divide2).isEqualTo(55);
        assertThat(divide3).isEqualTo(-11, Offset.offset(0.1));
    }

    @Test
    public void root() {

        double root1 = calculator.root(64);
        double root2 = calculator.root(0.01);
        double root3 = calculator.root(-(-121));
        assertThat(root1).isEqualTo(8);
        assertThat(root2).isNotEqualTo(78);
        assertThat(root3).isEqualTo(11);
    }

    @Test
    public void power(){
        double power1 = calculator.power(3, 3);
        double power2 = calculator.power(10, 2);
        double power3 = calculator.power(30, 4);
        assertThat(power1).isEqualTo(27);
        assertThat(power2).isEqualTo(100);
        assertThat(power3).isEqualTo(810_000);
    }

    @Test
    @Parameters({"10,2", "72,9", "0,1"})
    public void divisible(double valueA, double valueB) {
        assertThat(calculator.divisible(valueA, valueB)).isTrue();
    }

    @Test
    public void testArraySum(){
        double[] tab1 = {2, 4, 5, 6, 10, -1};
        double[] tab2 = {0.1, 987, 23.8, -132};
        assertThat(calculator.arraySum(tab1)).isNotNull().isEqualTo(26);
        assertThat(calculator.arraySum(tab2)).isNotNull().isEqualTo(878.9);
    }
}