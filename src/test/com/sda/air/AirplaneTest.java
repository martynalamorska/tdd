package com.sda.air;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AirplaneTest {

    private Airplane airplane1;
    private Airplane airplane2;

    @BeforeEach
    public void setup() {
        airplane1 = new Airplane("Airplane1", 3456);
        airplane2 = new Airplane("Airplane2", 3456);
    }

    @Test
    void ascent() {
        airplane1.ascent(996577);
        airplane2.ascent(1);
        assertEquals(1000000, airplane1.getHeight());
        assertEquals(3457, airplane2.getHeight());
    }

    @Test
    void descent() {
        airplane1.descent(-1);
        airplane2.descent(34789);
        assertEquals(3457, airplane1.getHeight());
        assertEquals(0, airplane2.getHeight());
    }

    @Test
    void getName() {
        assertEquals("Airplane1", airplane1.getName());
        assertEquals("Airplane2", airplane2.getName());
    }

    @Test
    void getHeight() {
        assertEquals(3456, airplane1.getHeight());
        assertEquals(3456, airplane2.getHeight());
    }
}