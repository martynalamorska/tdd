package com.sda.db;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class AuthenticationTest {

    private Database database;
    private List<User> userList;
    private Authentication authentication;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        userList = new ArrayList<>();
        userList.add(new User("u1", "1234"));
        userList.add(new User("u2", "5678"));
        userList.add(new User("u3", "9010"));
        database = new Database(userList);
        authentication = new Authentication(database);
    }

    @Test
    public void testLogin() {
        User user = new User("u2", "5678");
        authentication.login(user);
        assertThat(authentication.getLoggedUser()).isEqualTo("u2");
    }

    @Test
    public void testUserDoesNotExistException() {
        thrown.expect(UserDoesNotExistException.class);
        thrown.expectMessage("User doesn't exist");
        authentication.login(null);
    }

    @Test
    public void testWrongPasswordException() {
        thrown.expect(WrongPasswordException.class);
        thrown.expectMessage("Wrong password");
        authentication.login(new User("u2", "123"));
    }

    @Test
    public void testLogout() {
        User user = new User("u1", "1234");
        authentication.login(user);
        assertThat(authentication.getLoggedUser()).isEqualTo("u1");
        authentication.logout();
        assertThat(authentication.getLoggedUser()).isNull();
    }

}