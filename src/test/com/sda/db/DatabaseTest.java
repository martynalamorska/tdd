package com.sda.db;

import org.junit.Rule;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DatabaseTest {

    private Database database;
    private Authentication authentication;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        List<User> userList = new ArrayList<>();
        userList.add(new User("abcd", "1234"));
        userList.add(new User("efgh", "5678"));
        userList.add(new User("ijkl", "9010"));
        database = new Database(userList);
    }

    @Test
    public void testAddUser() {
        int sizeBefore = database.findAll().size();
        database.addUser(new User("newUser1", "newPw"));
        assertThat(database.findAll().size()).isEqualTo(sizeBefore + 1);
    }


    @Test
    public void testAddUserException() {
        thrown.expect(NullPointerException.class);
        database.addUser(null);
        database.findAll().forEach(user -> System.out.println(user.getLogin()));
    }

//    @Test
//    public void testAddUserException(){
//        try {
//            database.addUser(null);
//        } catch (Exception e) {
//            assertThat(e).isInstanceOf(NullPointerException.class);
//        }
//    }

    @Test
    public void testRemoveUser() {
        int sizeR = database.findAll().size();
        database.removeUser("abcd");
        database.findAll().forEach(userY -> System.out.println(userY.getLogin()));
        assertThat(database.findAll().size()).isEqualTo(sizeR - 1);
    }

    @Test
    public void testFindUser() {
        User user = new User("userToFind", "123");
        database.addUser(user);
        assertThat(database.findUser("userToFind")).isEqualTo(user);
        database.findAll().forEach(userX -> System.out.println(userX.getLogin()));
        System.out.println("Database size: " + database.findAll().size());
    }

    @Test
    public void testFindUserByNameFragment() {
        User user1 = new User("login", "password");
        User user2 = new User("login2", "password2");
        database.addUser(user1);
        database.addUser(user2);
        assertThat(database.findUserByNameFragment("lo")).isNotNull().contains(user1, user2);
    }

    @Test
    public void testModifyUser() {
        User user = new User("abcd", "098");
        database.modifyUser(user);
        assertThat(database.findUser(user.getLogin()).getPassword()).isEqualTo(user.getPassword());
        System.out.println(user);
    }

    @Test
    public void testUserDoesNotExistException() {
        thrown.expect(UserDoesNotExistException.class);
        database.modifyUser(null);
    }

}