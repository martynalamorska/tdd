package com.sda.db;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DatabaseMockTest {

    @Mock
    private Database database;

    @Before
    public void setup(){
        User user = new User("login", "password");
        when(database.findUser("login")).thenReturn(user);
        when(database.findUser("exception")).thenThrow(NullPointerException.class);
    }

    @Test
    public void testAdd(){
        User user = new User("login", "pw");
        database.addUser(user);
        verify(database, times(1)).addUser(user);
    }

    @Test
    public void test(){
        System.out.println(database.findUser("login").getLogin());
    }

    @Test
    public void testAddMore(){
        User user1 = new User("login", "password");
        User user2 = new User("login2", "password2");
        User user3 = new User("login3", "password3");
        database.addUser(user1);
        database.addUser(user2);
        database.addUser(user3);
        verify(database, times(3)).addUser(any());
    }

}
