package com.sda.db;

public class Authentication {

    private String loggedUser;
    private Database database;

    public Authentication(Database database) {
        this.database = database;
    }

    public String getLoggedUser() {
        return loggedUser;
    }

    public void login(User user) {
        if (user == null || !database.getUsers().keySet().contains(user.getLogin())) {
            throw new UserDoesNotExistException("User doesn't exist");
        } else if (!database.findUser(user.getLogin()).getPassword().equals(user.getPassword())) {
            throw new WrongPasswordException("Wrong password");
        }
        loggedUser = user.getLogin();
    }

    public void logout() {
        loggedUser = null;
    }

}
