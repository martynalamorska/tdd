package com.sda.db;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Database {
    private Map<String, User> users;

    public Database() {
        users = new HashMap<String, User>();
    }

    public Map<String, User> getUsers() {
        return users;
    }

    public Database(List<User> userList) {
        users = userList.stream().collect(Collectors.toMap(User::getLogin, Function.identity()));
    }

    public void addUser(User user) {
        if (users.keySet().contains(user.getLogin())) {
            throw new UserExistsException("User already exists.");
        }
        users.put(user.getLogin(), user);
    }

    public List<User> findAll() {
        return new ArrayList<>(users.values());
    }

    public void removeUser(String login) {
        users.remove(login);
    }

    public User findUser(String login) {
        return users.get(login);
    }

    public List<User> findUserByNameFragment(String nameFragment) {
        List<User> userList = new ArrayList<>();
        for (User x : users.values()) {
            if (x.getLogin().contains(nameFragment)) {
                userList.add(x);
            }
        }
        return userList;
//        for (String key : users.keySet()) {
//            if (key.contains(nameFragment)) {
//                userList.add(users.get(key));
//            }
//        }
//        return userList;
    }

    public void modifyUser(User user) {
        if (user == null || !users.keySet().contains(user.getLogin())) {
            throw new UserDoesNotExistException("User doesn't exist");
        }
//        users.get(user.getLogin()).setPassword(user.getPassword());
        users.put(user.getLogin(), user);
    }

}
