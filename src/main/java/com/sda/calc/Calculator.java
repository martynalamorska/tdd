package com.sda.calc;

import java.util.Arrays;

/**
 * Created by katar on 06.12.2018.
 */
public class Calculator {

    public Double add(double a, double b) {
        return a + b;
    }

    public Double subtract(double a, double b) {
        return a - b;
    }

    public Double multiply(double a, double b) {
        return a * b;
    }

    public Double divide(double a, double b) {
        return a / b;
    }

    public Double root(double a) {
        if (a < 0) {
            throw new RootException();
        }
        return Math.sqrt(a);
    }

    public Double power(double a, double b){
        return Math.pow(a, b);
    }

    public boolean divisible(double a, double b) {
        return a % b == 0;
    }

    public Double arraySum(double[] tab){
        double sum = 0;
        for (int i = 0; i < tab.length; i++) {
            sum = sum + tab[i]; //sum+=tab[i]
        }
        return sum;
        // return Array.stream(tab).sum();
    }

}
